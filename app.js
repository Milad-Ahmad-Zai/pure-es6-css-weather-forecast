const APIKEY = "610d60886155afea4411c94798c200db";
const URL = "http://api.openweathermap.org/data/2.5/forecast?";
const imgUrl = "http://openweathermap.org/img/w/"

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      lat = String(position.coords.latitude);
      lon = String(position.coords.longitude);
      getWeather(lat, lon);
    });
  }else{
    console.log('no location');
  }
}

function getWeather(lat, lon) {
  let xhr = new XMLHttpRequest();
  xhr.open("GET",`${URL}lat=${lat}&lon=${lon}&units=metric&APPID=${APIKEY}`,false);
  xhr.send(); 

  sortDays(JSON.parse(xhr.response));
}


function sortDays(data) {
  console.log(data);
  document.getElementById("heading").innerHTML = `Forecast for <span class='location'>${data.city.name}, ${data.city.country}</span>`;
  let days = [];
  let day = getDay(data.list[0].dt_txt);
  for (let i = 0; i < data.list.length; i++) {
    if(getDay(data.list[i].dt_txt) !== day)
      day = getDay(data.list[i].dt_txt);
    days[day] = days[day] || [];
    days[day].push(data.list[i]);
  } 
  updateDisplay(days);
}


function updateDisplay(days){
  for(let day in days){
    console.log(days[day])
    let sum = days[day].map(x => x.main.temp).reduce(function(a, b) { return a + b; });
    let avg = sum / days[day].length;
    
    let html = `
      <span class="temp">${avg.toFixed(0)}</span>
      <span class="deg">°C</span>
      <img class="weather-icon" src="${imgUrl}${days[day][0].weather[0].icon}.png" />
      <span class="day">${day}</span>
    `;

    let elm = document.createElement("div");
    elm.innerHTML = html;
    elm.className = "box";
    document.getElementById("forecast").appendChild(elm);
  };
}

function getDay(dateString) {
  var week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return week[new Date(dateString).getDay()];
}

getLocation();